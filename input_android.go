// +build android

package gamepad

// #cgo LDFLAGS: -landroid
// #include "input_android.h"
import "C"
import (
	"math"
	"unsafe"

	"golang.org/x/mobile/app"
)

var (
	buttonMap = map[C.jint]Button{
		C.AKEYCODE_DPAD_CENTER:     BtnUnknown,
		C.AKEYCODE_DPAD_UP:         BtnDUp,
		C.AKEYCODE_DPAD_DOWN:       BtnDDown,
		C.AKEYCODE_DPAD_LEFT:       BtnDLeft,
		C.AKEYCODE_DPAD_RIGHT:      BtnDRight,
		C.AKEYCODE_DPAD_UP_LEFT:    BtnUnknown,
		C.AKEYCODE_DPAD_UP_RIGHT:   BtnUnknown,
		C.AKEYCODE_DPAD_DOWN_LEFT:  BtnUnknown,
		C.AKEYCODE_DPAD_DOWN_RIGHT: BtnUnknown,
		C.AKEYCODE_BUTTON_A:        BtnA,
		C.AKEYCODE_BUTTON_B:        BtnB,
		C.AKEYCODE_BUTTON_C:        BtnUnknown,
		C.AKEYCODE_BUTTON_X:        BtnX,
		C.AKEYCODE_BUTTON_Y:        BtnY,
		C.AKEYCODE_BUTTON_Z:        BtnUnknown,
		C.AKEYCODE_BUTTON_START:    BtnStart,
		C.AKEYCODE_BUTTON_SELECT:   BtnSelect,
		C.AKEYCODE_BUTTON_MODE:     BtnMode,
		C.AKEYCODE_BUTTON_L1:       BtnL1,
		C.AKEYCODE_BUTTON_R1:       BtnR1,
		C.AKEYCODE_BUTTON_L2:       BtnL2,
		C.AKEYCODE_BUTTON_R2:       BtnR2,
		C.AKEYCODE_BUTTON_THUMBL:   BtnLeftStick,
		C.AKEYCODE_BUTTON_THUMBR:   BtnRightStick,
		C.AKEYCODE_BUTTON_1:        BtnUnknown,
		C.AKEYCODE_BUTTON_2:        BtnUnknown,
		C.AKEYCODE_BUTTON_3:        BtnUnknown,
		C.AKEYCODE_BUTTON_4:        BtnUnknown,
		C.AKEYCODE_BUTTON_5:        BtnUnknown,
		C.AKEYCODE_BUTTON_6:        BtnUnknown,
		C.AKEYCODE_BUTTON_7:        BtnUnknown,
		C.AKEYCODE_BUTTON_8:        BtnUnknown,
		C.AKEYCODE_BUTTON_9:        BtnUnknown,
		C.AKEYCODE_BUTTON_10:       BtnUnknown,
		C.AKEYCODE_BUTTON_11:       BtnUnknown,
		C.AKEYCODE_BUTTON_12:       BtnUnknown,
		C.AKEYCODE_BUTTON_13:       BtnUnknown,
		C.AKEYCODE_BUTTON_14:       BtnUnknown,
		C.AKEYCODE_BUTTON_15:       BtnUnknown,
		C.AKEYCODE_BUTTON_16:       BtnUnknown,
	}

	axisMap = map[C.jint]Axis{
		C.AMOTION_EVENT_AXIS_X:          AxisLeftX,
		C.AMOTION_EVENT_AXIS_Y:          AxisLeftY,
		C.AMOTION_EVENT_AXIS_Z:          AxisRightX,
		C.AMOTION_EVENT_AXIS_RZ:         AxisRightY,
		C.AMOTION_EVENT_AXIS_HAT_X:      AxisUnknown - 1,
		C.AMOTION_EVENT_AXIS_HAT_Y:      AxisUnknown - 1,
		C.AMOTION_EVENT_AXIS_LTRIGGER:   AxisUnknown - 1,
		C.AMOTION_EVENT_AXIS_RTRIGGER:   AxisUnknown - 1,
		C.AMOTION_EVENT_AXIS_BRAKE:      AxisUnknown,
		C.AMOTION_EVENT_AXIS_GAS:        AxisUnknown,
		C.AMOTION_EVENT_AXIS_RUDDER:     AxisUnknown,
		C.AMOTION_EVENT_AXIS_RX:         AxisUnknown,
		C.AMOTION_EVENT_AXIS_RY:         AxisUnknown,
		C.AMOTION_EVENT_AXIS_THROTTLE:   AxisUnknown,
		C.AMOTION_EVENT_AXIS_WHEEL:      AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_1:  AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_2:  AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_3:  AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_4:  AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_5:  AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_6:  AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_7:  AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_8:  AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_9:  AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_10: AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_11: AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_12: AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_13: AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_14: AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_15: AxisUnknown,
		C.AMOTION_EVENT_AXIS_GENERIC_16: AxisUnknown,
	}

	analogButtonMap = map[C.jint][2]Button{
		C.AMOTION_EVENT_AXIS_HAT_X:    {BtnDLeft, BtnDRight},
		C.AMOTION_EVENT_AXIS_HAT_Y:    {BtnDUp, BtnDDown},
		C.AMOTION_EVENT_AXIS_LTRIGGER: {BtnUnknown - 1, BtnL2},
		C.AMOTION_EVENT_AXIS_RTRIGGER: {BtnUnknown - 1, BtnR2},
	}
)

func update() error {
	nextDevices = nextDevices[:0]

	return app.RunOnJVM(func(vm, jniEnv, ctx uintptr) error {
		C.deviceListUpdate((*C.JNIEnv)(unsafe.Pointer(jniEnv)))

		return nil
	})
}

type gamepad struct {
	device     C.jint
	descriptor string
	buttonMap  []C.jint
	axisMap    []C.jint
	axisIdx    []int
	axisBtn    []int
	button     []bool
	axis       []float32
}

//export storeDeviceDescriptor
func storeDeviceDescriptor(device C.jint, descriptor *C.cchar) C.jboolean {
	desc := C.GoString(descriptor)

	for _, g := range devices {
		if g.device == device && g.descriptor == desc {
			nextDevices = append(nextDevices, g)

			return C.JNI_FALSE
		}
	}

	nextDevices = append(nextDevices, &Gamepad{
		gamepad: gamepad{
			device:     device,
			descriptor: desc,
		},
	})

	return C.JNI_TRUE
}

//export addDeviceButtons
func addDeviceButtons(device C.jint, buttons *C.jboolean) {
	buttonSupported := *(*[len(C.knownButtons)]C.jboolean)(unsafe.Pointer(buttons))

	for _, g := range nextDevices {
		if g.device != device {
			continue
		}

		for i, supported := range buttonSupported {
			if supported != C.JNI_FALSE {
				g.buttonMap = append(g.buttonMap, C.knownButtons[i])
				g.button = append(g.button, false)
			}
		}

		g.axisBtn = make([]int, len(g.button))

		break
	}
}

//export addDeviceAxis
func addDeviceAxis(device, axis C.jint) {
	for _, g := range nextDevices {
		if g.device != device {
			continue
		}

		g.axisMap = append(g.axisMap, axis)
		g.axis = append(g.axis, 0)

		if a, ok := axisMap[axis]; ok && a == AxisUnknown-1 {
			g.addAxisButton(-len(g.axis), analogButtonMap[axis][0])
			g.addAxisButton(len(g.axis), analogButtonMap[axis][1])
		} else {
			g.axisIdx = append(g.axisIdx, len(g.axis)-1)
		}

		break
	}
}

func (g *gamepad) addAxisButton(axis int, btn Button) {
	switch btn {
	case BtnUnknown - 1:
		// ignored
	case BtnUnknown:
		g.axisBtn = append(g.axisBtn, axis)
	default:
		found := false

		for i, b := range g.buttonMap {
			if mappedBtn, ok := buttonMap[b]; ok && mappedBtn == btn && g.axisBtn[i] == 0 {
				g.axisBtn[i] = axis
				found = true

				break
			}
		}

		if !found {
			g.axisBtn = append(g.axisBtn, axis)
		}
	}
}

// OnInputEvent must be called when the app receives a C.AInputEvent to update
// the state of the gamepad buttons and axes.
func OnInputEvent(inputEvent unsafe.Pointer) {
	event := (*C.AInputEvent)(inputEvent)

	source := C.AInputEvent_getSource(event)
	if source != C.AINPUT_SOURCE_GAMEPAD && source != C.AINPUT_SOURCE_JOYSTICK {
		return
	}

	deviceLock.Lock()
	defer deviceLock.Unlock()

	device := C.jint(C.AInputEvent_getDeviceId(event))
	for _, g := range devices {
		if g.device != device {
			continue
		}

		switch C.AInputEvent_getType(event) {
		case C.AINPUT_EVENT_TYPE_KEY:
			var state bool

			switch C.AKeyEvent_getAction(event) {
			case C.AKEY_EVENT_ACTION_DOWN:
				state = true
			case C.AKEY_EVENT_ACTION_UP:
				state = false
			default:
				return
			}

			button := C.jint(C.AKeyEvent_getKeyCode(event))

			for i, btn := range g.buttonMap {
				if btn == button {
					g.button[i] = state

					break
				}
			}
		case C.AINPUT_EVENT_TYPE_MOTION:
			for i, axis := range g.axisMap {
				g.axis[i] = float32(C.AMotionEvent_getAxisValue(event, axis, 0))
			}
		}

		break
	}
}

func (g *gamepad) getName() string {
	return g.descriptor
}

func (g *gamepad) update() error {
	return nil
}

func (g *gamepad) numAxis() int {
	return len(g.axisIdx)
}

func (g *gamepad) numButton() int {
	return len(g.axisBtn)
}

func (g *gamepad) getAxis(index int) float32 {
	if uint(index) >= uint(len(g.axisIdx)) {
		return float32(math.NaN())
	}

	deviceLock.Lock()
	defer deviceLock.Unlock()

	return g.axis[g.axisIdx[index]]
}

func (g *gamepad) getButton(index int) (float32, bool) {
	if uint(index) >= uint(len(g.button)) {
		return float32(math.NaN()), false
	}

	deviceLock.Lock()
	defer deviceLock.Unlock()

	value, pressed := float32(0), false

	switch a := g.axisBtn[index]; {
	case a == 0:
		if g.button[index] {
			value = 1
		}
	case a < 0:
		if v := g.axis[1-a]; v < 0 {
			value = -v
		}
	default:
		if v := g.axis[a-1]; v > 0 {
			value = v
		}
	}

	if index < len(g.button) {
		pressed = g.button[index]
	} else {
		pressed = value >= 0.5
	}

	return value, pressed
}

func (g *gamepad) indexToAxis(index int) Axis {
	if uint(index) >= uint(len(g.axisIdx)) {
		return AxisUnknown
	}

	if a, ok := axisMap[g.axisMap[g.axisIdx[index]]]; ok {
		return a
	}

	return AxisUnknown
}

func (g *gamepad) axisToIndex(axis Axis) int {
	if axis == AxisUnknown {
		return -1
	}

	for i, ai := range g.axisIdx {
		if a, ok := axisMap[g.axisMap[ai]]; ok && a == axis {
			return i
		}
	}

	return -1
}

func (g *gamepad) indexToButton(index int) Button {
	if uint(index) >= uint(len(g.axisBtn)) {
		return BtnUnknown
	}

	if uint(index) < uint(len(g.buttonMap)) {
		if b, ok := buttonMap[g.buttonMap[index]]; ok {
			return b
		}

		return BtnUnknown
	}

	var axis, dir int

	if g.axisBtn[index] < 0 {
		axis, dir = 1-g.axisBtn[index], 0
	} else {
		axis, dir = g.axisBtn[index]-1, 1
	}

	return analogButtonMap[g.axisMap[axis]][dir]
}

func (g *gamepad) buttonToIndex(btn Button) int {
	if btn == BtnUnknown {
		return -1
	}

	for i, axis := range g.axisBtn {
		switch {
		case axis == 0:
			if b, ok := buttonMap[g.buttonMap[i]]; ok && b == btn {
				return i
			}
		case axis < 0:
			if analogButtonMap[g.axisMap[1-axis]][0] == btn {
				return i
			}
		default:
			if analogButtonMap[g.axisMap[axis-1]][1] == btn {
				return i
			}
		}
	}

	return -1
}
