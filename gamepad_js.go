// +build js

package gamepad

import (
	"math"
	"syscall/js"
)

type gamepad struct {
	value js.Value
}

func init() {
	win := js.Global()

	win.Call("addEventListener", "gamepadconnected", js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		gp := args[0].Get("gamepad")

		deviceLock.Lock()
		defer deviceLock.Unlock()

		for _, g := range nextDevices {
			if g.value.Equal(gp) {
				// already have
				return js.Undefined()
			}
		}

		nextDevices = append(nextDevices, &Gamepad{
			gamepad: gamepad{
				value: gp,
			},
		})

		return js.Undefined()
	}))

	win.Call("addEventListener", "gamepaddisconnected", js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		gamepad := args[0].Get("gamepad")

		deviceLock.Lock()
		defer deviceLock.Unlock()

		for i, g := range nextDevices {
			if g.value.Equal(gamepad) {
				nextDevices = append(nextDevices[:i], nextDevices[i+1:]...)

				break
			}
		}

		return js.Undefined()
	}))

	deviceLock.Lock()
	defer deviceLock.Unlock()

	defer func() {
		// If we aren't allowed to use the "gamepad" permission,
		// getGamepads will throw a SecurityError DOMException.
		//
		// Treat this case as "there are no gamepads" rather than
		// crashing at startup.
		if r := recover(); r != nil {
			_ = r.(js.Error)
		}
	}()

	nav := win.Get("navigator")
	if nav.Get("getGamepads").Truthy() {
		gamepads := nav.Call("getGamepads")
		for i := 0; i < gamepads.Length(); i++ {
			if g := gamepads.Index(i); g.Truthy() {
				nextDevices = append(nextDevices, &Gamepad{gamepad: gamepad{value: g}})
			}
		}
	}
}

func update() error {
	// not needed on js
	return nil
}

func (g *gamepad) update() error {
	// Chrome doesn't deliver updates unless we reobtain the gamepad every frame:
	gp := js.Global().Get("navigator").Call("getGamepads")
	gp = gp.Index(g.value.Get("index").Int())

	if gp.Truthy() {
		g.value = gp
	}

	return nil
}

func (g *gamepad) getName() string {
	return g.value.Get("id").String()
}

func (g *gamepad) numAxis() int {
	return g.value.Get("axes").Length()
}

func (g *gamepad) numButton() int {
	return g.value.Get("buttons").Length()
}

func (g *gamepad) getAxis(index int) float32 {
	if uint(index) >= uint(g.numAxis()) {
		return float32(math.NaN())
	}

	return float32(g.value.Get("axes").Index(index).Float())
}

func (g *gamepad) getButton(index int) (float32, bool) {
	if uint(index) >= uint(g.numButton()) {
		return float32(math.NaN()), false
	}

	btn := g.value.Get("buttons").Index(index)
	return float32(btn.Get("value").Float()), btn.Get("pressed").Bool()
}

const mappingStandard = "standard"

func (g *gamepad) indexToAxis(index int) Axis {
	if g.value.Get("mapping").String() != mappingStandard {
		return AxisUnknown
	}

	if uint(index) > uint(AxisRightY) {
		return AxisUnknown
	}

	if uint(index) < uint(g.value.Get("axes").Length()) {
		return Axis(index)
	}

	return AxisUnknown
}

func (g *gamepad) axisToIndex(axis Axis) int {
	if g.value.Get("mapping").String() != mappingStandard {
		return -1
	}

	if axis > AxisRightY {
		return -1
	}

	if uint(axis) < uint(g.value.Get("axes").Length()) {
		return int(axis)
	}

	return -1
}

func (g *gamepad) indexToButton(index int) Button {
	if g.value.Get("mapping").String() != mappingStandard {
		return BtnUnknown
	}

	if uint(index) > uint(BtnMode) {
		return BtnUnknown
	}

	if uint(index) < uint(g.value.Get("buttons").Length()) {
		return Button(index)
	}

	return BtnUnknown
}

func (g *gamepad) buttonToIndex(btn Button) int {
	if g.value.Get("mapping").String() != mappingStandard {
		return -1
	}

	if btn > BtnMode {
		return -1
	}

	if uint(btn) < uint(g.value.Get("buttons").Length()) {
		return int(btn)
	}

	return -1
}
