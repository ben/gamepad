#include <android/input.h>
#include <jni.h>

#define NUM_KNOWN_BUTTONS 40
#define NUM_KNOWN_AXES 31

extern const jint knownButtons[NUM_KNOWN_BUTTONS];
extern const jint knownAxes[NUM_KNOWN_AXES];

void deviceListUpdate(JNIEnv *env);

typedef const char cchar;
