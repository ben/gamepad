// Package gamepad implements access to the system's gamepad or joystick API.
//
// This package currently supports windows, linux, android, and js/wasm.
package gamepad

type Gamepad struct {
	gamepad
}

// Name returns an identifier for this gamepad. It does not change,
// although multiple gamepads may have the same name.
func (g *Gamepad) Name() string {
	return g.getName()
}

// NumAxis returns the number of axes on the gamepad.
func (g *Gamepad) NumAxis() int {
	return g.numAxis()
}

// Axis returns the current state of the given axis on the gamepad.
//
// If index is not in [0, NumAxis), this method returns NaN.
func (g *Gamepad) Axis(index int) float32 {
	return g.getAxis(index)
}

// NumButton returns the number of buttons on the gamepad.
func (g *Gamepad) NumButton() int {
	return g.numButton()
}

// Button returns the current state of the given button on the gamepad.
//
// If index is not in [0, NumButton), this method returns NaN, false.
//
// If the platform does not support variable button presses, the
// value of the float is either 0 or 1.
func (g *Gamepad) Button(index int) (float32, bool) {
	return g.getButton(index)
}

// IndexToAxis returns the standard axis at the specified index.
func (g *Gamepad) IndexToAxis(index int) Axis {
	return g.indexToAxis(index)
}

// AxisToIndex returns the index of the specified standard axis, or -1 if the
// axis is not supported by this gamepad or the axis is AxisUnknown.
func (g *Gamepad) AxisToIndex(axis Axis) int {
	return g.axisToIndex(axis)
}

// IndexToButton returns the standard button at the specified index.
func (g *Gamepad) IndexToButton(index int) Button {
	return g.indexToButton(index)
}

// ButtonToIndex returns the index of the specified standard button, or -1 if
// the button is not supported by this gamepad or the button is BtnUnknown.
func (g *Gamepad) ButtonToIndex(btn Button) int {
	return g.buttonToIndex(btn)
}
