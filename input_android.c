#include "input_android.h"
#include "_cgo_export.h"

const jint knownButtons[] = {
	// d-pad
	AKEYCODE_DPAD_CENTER,
	AKEYCODE_DPAD_UP,
	AKEYCODE_DPAD_DOWN,
	AKEYCODE_DPAD_LEFT,
	AKEYCODE_DPAD_RIGHT,
	AKEYCODE_DPAD_UP_LEFT,
	AKEYCODE_DPAD_UP_RIGHT,
	AKEYCODE_DPAD_DOWN_LEFT,
	AKEYCODE_DPAD_DOWN_RIGHT,
	// face buttons
	AKEYCODE_BUTTON_A,
	AKEYCODE_BUTTON_B,
	AKEYCODE_BUTTON_C,
	AKEYCODE_BUTTON_X,
	AKEYCODE_BUTTON_Y,
	AKEYCODE_BUTTON_Z,
	// center
	AKEYCODE_BUTTON_START,
	AKEYCODE_BUTTON_SELECT,
	AKEYCODE_BUTTON_MODE,
	// triggers
	AKEYCODE_BUTTON_L1,
	AKEYCODE_BUTTON_R1,
	AKEYCODE_BUTTON_L2,
	AKEYCODE_BUTTON_R2,
	// sticks
	AKEYCODE_BUTTON_THUMBL,
	AKEYCODE_BUTTON_THUMBR,
	// unknown
	AKEYCODE_BUTTON_1,
	AKEYCODE_BUTTON_2,
	AKEYCODE_BUTTON_3,
	AKEYCODE_BUTTON_4,
	AKEYCODE_BUTTON_5,
	AKEYCODE_BUTTON_6,
	AKEYCODE_BUTTON_7,
	AKEYCODE_BUTTON_8,
	AKEYCODE_BUTTON_9,
	AKEYCODE_BUTTON_10,
	AKEYCODE_BUTTON_11,
	AKEYCODE_BUTTON_12,
	AKEYCODE_BUTTON_13,
	AKEYCODE_BUTTON_14,
	AKEYCODE_BUTTON_15,
	AKEYCODE_BUTTON_16,
};

const jint knownAxes[] = {
	// left stick
	AMOTION_EVENT_AXIS_X,
	AMOTION_EVENT_AXIS_Y,
	// right stick
	AMOTION_EVENT_AXIS_Z,
	AMOTION_EVENT_AXIS_RZ,
	// d-pad
	AMOTION_EVENT_AXIS_HAT_X,
	AMOTION_EVENT_AXIS_HAT_Y,
	// triggers
	AMOTION_EVENT_AXIS_LTRIGGER,
	AMOTION_EVENT_AXIS_RTRIGGER,
	// non-gamepad joysticks
	AMOTION_EVENT_AXIS_BRAKE,
	AMOTION_EVENT_AXIS_GAS,
	AMOTION_EVENT_AXIS_RUDDER,
	AMOTION_EVENT_AXIS_RX,
	AMOTION_EVENT_AXIS_RY,
	AMOTION_EVENT_AXIS_THROTTLE,
	AMOTION_EVENT_AXIS_WHEEL,
	// unknown
	AMOTION_EVENT_AXIS_GENERIC_1,
	AMOTION_EVENT_AXIS_GENERIC_2,
	AMOTION_EVENT_AXIS_GENERIC_3,
	AMOTION_EVENT_AXIS_GENERIC_4,
	AMOTION_EVENT_AXIS_GENERIC_5,
	AMOTION_EVENT_AXIS_GENERIC_6,
	AMOTION_EVENT_AXIS_GENERIC_7,
	AMOTION_EVENT_AXIS_GENERIC_8,
	AMOTION_EVENT_AXIS_GENERIC_9,
	AMOTION_EVENT_AXIS_GENERIC_10,
	AMOTION_EVENT_AXIS_GENERIC_11,
	AMOTION_EVENT_AXIS_GENERIC_12,
	AMOTION_EVENT_AXIS_GENERIC_13,
	AMOTION_EVENT_AXIS_GENERIC_14,
	AMOTION_EVENT_AXIS_GENERIC_15,
	AMOTION_EVENT_AXIS_GENERIC_16,
};

void deviceListUpdate(JNIEnv *env) {
	jintArray knownButtonsJava = (jintArray)(*env)->NewIntArray(env, sizeof(knownButtons) / sizeof(knownButtons[0]));
	(*env)->SetIntArrayRegion(env, knownButtonsJava, 0, sizeof(knownButtons) / sizeof(knownButtons[0]), &knownButtons[0]);

	jclass inputDevice = (*env)->FindClass(env, "android/view/InputDevice");
	jmethodID getDeviceIDs = (*env)->GetStaticMethodID(env, inputDevice, "getDeviceIds", "()[I");
	jmethodID getDevice = (*env)->GetStaticMethodID(env, inputDevice, "getDevice", "(I)Landroid/view/InputDevice;");
	jmethodID getDescriptor = (*env)->GetMethodID(env, inputDevice, "getDescriptor", "()Ljava/lang/String;");
	jmethodID supportsSource = (*env)->GetMethodID(env, inputDevice, "supportsSource", "(I)Z");
	jmethodID hasKeys = (*env)->GetMethodID(env, inputDevice, "hasKeys", "([I)[Z");
	jmethodID getMotionRange = (*env)->GetMethodID(env, inputDevice, "getMotionRange", "(I)Landroid/view/InputDevice$MotionRange;");

	jintArray deviceIDsArr = (jintArray)(*env)->CallStaticObjectMethod(env, inputDevice, getDeviceIDs);
	jsize numDeviceIDs = (*env)->GetArrayLength(env, (jarray)deviceIDsArr);
	jint *deviceIDs = (*env)->GetIntArrayElements(env, deviceIDsArr, NULL);
	for (jint *deviceID = deviceIDs; numDeviceIDs; numDeviceIDs--, deviceID++) {
		jobject dev = (*env)->CallStaticObjectMethod(env, inputDevice, getDevice, *deviceID);
		if ((*env)->CallBooleanMethod(env, dev, supportsSource, (jint)(AINPUT_SOURCE_GAMEPAD)) || (*env)->CallBooleanMethod(env, dev, supportsSource, (jint)(AINPUT_SOURCE_JOYSTICK))) {
			jstring descriptorStr = (jstring)(*env)->CallObjectMethod(env, dev, getDescriptor);
			const char *descriptor = (*env)->GetStringUTFChars(env, descriptorStr, NULL);
			jboolean isNew = storeDeviceDescriptor(*deviceID, descriptor);
			(*env)->ReleaseStringUTFChars(env, descriptorStr, descriptor);
			(*env)->DeleteLocalRef(env, descriptorStr);

			if (isNew) {
				jboolean supported[sizeof(knownButtons) / sizeof(knownButtons[0])];
				jbooleanArray supportedButtons = (jbooleanArray)(*env)->CallObjectMethod(env, dev, hasKeys, knownButtonsJava);
				(*env)->GetBooleanArrayRegion(env, supportedButtons, 0, sizeof(supported) / sizeof(supported[0]), &supported[0]);
				(*env)->DeleteLocalRef(env, supportedButtons);
				addDeviceButtons(*deviceID, &supported[0]);

				for (size_t i = 0; i < sizeof(knownAxes) / sizeof(knownAxes[0]); i++) {
					jobject axisRange = (*env)->CallObjectMethod(env, dev, getMotionRange, knownAxes[0]);
					if (!(*env)->IsSameObject(env, axisRange, NULL)) {
						addDeviceAxis(*deviceID, knownAxes[i]);
					}
					(*env)->DeleteLocalRef(env, axisRange);
				}
			}
		}
		(*env)->DeleteLocalRef(env, dev);
	}
	(*env)->ReleaseIntArrayElements(env, deviceIDsArr, deviceIDs, JNI_ABORT);
}
