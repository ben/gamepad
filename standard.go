//go:generate go run golang.org/x/tools/cmd/stringer -type Button -trimprefix Btn
//go:generate go run golang.org/x/tools/cmd/stringer -type Axis -trimprefix Axis

package gamepad

// Button is a pressable switch on the gamepad. Some buttons may support analog
// input from 0 to +1, where 0 is fully released and +1 is fully pressed.
type Button int

const (
	// BtnUnknown is any button not listed in this enumeration, or any
	// button on a gamepad that does not have a known layout.
	BtnUnknown Button = iota - 1
	// BtnA is the bottom button on the right side of the gamepad, usually
	// labelled "A" or "Cross".
	BtnA
	// BtnB is the right button on the right side of the gamepad, usually
	// labelled "B" or "Circle".
	BtnB
	// BtnX is the left button on the right side of the gamepad, usually
	// labelled "X" or "Square".
	BtnX
	// BtnY is the top button on the right side of the gamepad, usually
	// labelled "Y" or "Triangle".
	BtnY
	// BtnL1 is the top left button on the top of the gamepad, usually
	// labelled "L", "L1", or "LB".
	BtnL1
	// BtnR1 is the top right button on the top of the gamepad, usually
	// labelled "R", "R1", or "RB".
	BtnR1
	// BtnL2 is the bottom right button on the top of the gamepad, usually
	// labelled "ZL", "L2", or "LT".
	BtnL2
	// BtnR2 is the bottom right button on the top of the gamepad, usually
	// labelled "ZR", "R2", or "RT".
	BtnR2
	// BtnSelect is the left button in the middle of the gamepad, usually
	// labelled "Select", "Back", or "Minus".
	BtnSelect
	// BtnStart is the right button in the middle of the gamepad, usually
	// labelled "Start", "Forward", or "Plus".
	BtnStart
	// BtnLeftStick is the left directional stick, usually labelled "LSB"
	// or "L3".
	BtnLeftStick
	// BtnRightStick is the right directional stick, usually labelled "RSB"
	// or "R3".
	BtnRightStick
	// BtnDUp is the up direction on the d-pad.
	BtnDUp
	// BtnDDown is the down direction on the d-pad.
	BtnDDown
	// BtnDLeft is the left direction on the d-pad.
	BtnDLeft
	// BtnDRight is the right direction on the d-pad.
	BtnDRight
	// BtnMode is the center button in the middle of the gamepad, usually
	// labelled with a logo or "Share" or "Capture".
	BtnMode
)

// Axis is an analog directional input from -1 to +1. Some AxisUnknown axes
// may be from 0 to +1 instead.
type Axis int

const (
	// AxisUnknown is any axis not listed in this enumeration, or any
	// axis on a gamepad that does not have a known layout.
	AxisUnknown Axis = iota - 1
	// AxisLeftX is the left-right tilt of the left stick, with -1 as full
	// left and +1 as full right.
	AxisLeftX
	// AxisLeftY is the up-down tilt of the left stick, with -1 as full
	// up and +1 as full down.
	AxisLeftY
	// AxisRightX is the left-right tilt of the right stick, with -1 as full
	// left and +1 as full right.
	AxisRightX
	// AxisRightY is the up-down tilt of the right stick, with -1 as full
	// up and +1 as full down.
	AxisRightY
)
