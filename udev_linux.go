// This file is heavily based on the Mozilla Firefox implementation.
// Original copyright notice follows:

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// +build linux,!android

package gamepad

// #cgo LDFLAGS: -ludev
// #include <libudev.h>
// const char *cstring_udev = "udev";
// const char *cstring_input = "input";
// const char *cstring_ID_INPUT_JOYSTICK = "ID_INPUT_JOYSTICK";
// const char *cstring_ID_VENDOR_ID = "ID_VENDOR_ID";
// const char *cstring_ID_MODEL_ID = "ID_MODEL_ID";
// const char *cstring_id_vendor = "id/vendor";
// const char *cstring_id_product = "id/product";
import "C"
import (
	"strings"
)

const joystickPrefix = "/dev/input/js"

func init() {
	udev := C.udev_new()
	if udev == nil {
		// failed to initialize libudev - we won't be able to discover gamepads
		return
	}

	initMonitor(udev)
	scanDevices(udev)
}

func initMonitor(udev *C.struct_udev) {
	monitor := C.udev_monitor_new_from_netlink(udev, C.cstring_udev)
	if monitor == nil {
		return
	}

	C.udev_monitor_filter_add_match_subsystem_devtype(monitor, C.cstring_input, nil)

	go readMonitor(monitor)

	C.udev_monitor_enable_receiving(monitor)
}

func readMonitor(monitor *C.struct_udev_monitor) {
	for {
		dev := C.udev_monitor_receive_device(monitor)
		if isGamepad(dev) {
			action := C.GoString(C.udev_device_get_action(dev))
			if action == "add" {
				addDevice(dev)
			} else if action == "remove" {
				removeDevice(dev)
			}
		}

		C.udev_device_unref(dev)
	}
}

func scanDevices(udev *C.struct_udev) {
	e := C.udev_enumerate_new(udev)
	C.udev_enumerate_add_match_subsystem(e, C.cstring_input)
	C.udev_enumerate_scan_devices(e)

	for entry := C.udev_enumerate_get_list_entry(e); entry != nil; entry = C.udev_list_entry_get_next(entry) {
		path := C.udev_list_entry_get_name(entry)
		dev := C.udev_device_new_from_syspath(udev, path)
		if isGamepad(dev) {
			addDevice(dev)
		}

		C.udev_device_unref(dev)
	}

	C.udev_enumerate_unref(e)
}

func isGamepad(dev *C.struct_udev_device) bool {
	if C.udev_device_get_property_value(dev, C.cstring_ID_INPUT_JOYSTICK) == nil {
		return false
	}

	devpath := C.udev_device_get_devnode(dev)
	if devpath == nil {
		return false
	}

	return strings.HasPrefix(C.GoString(devpath), joystickPrefix)
}

func addDevice(dev *C.struct_udev_device) {
	devpath := C.udev_device_get_devnode(dev)
	if devpath == nil {
		return
	}

	path := C.GoString(devpath)

	deviceLock.Lock()
	defer deviceLock.Unlock()

	for _, g := range nextDevices {
		if g.path == path {
			// already have it
			return
		}
	}

	g, err := open(path)
	if err != nil {
		return
	}

	vendor := C.udev_device_get_property_value(dev, C.cstring_ID_VENDOR_ID)
	model := C.udev_device_get_property_value(dev, C.cstring_ID_MODEL_ID)
	if vendor == nil || model == nil {
		parent := C.udev_device_get_parent_with_subsystem_devtype(dev, C.cstring_input, nil)

		if parent != nil {
			vendor = C.udev_device_get_sysattr_value(parent, C.cstring_id_vendor)
			model = C.udev_device_get_sysattr_value(parent, C.cstring_id_product)
		}
	}

	if vendor != nil {
		g.vendor = C.GoString(vendor)
	}

	if model != nil {
		g.model = C.GoString(model)
	}

	nextDevices = append(nextDevices, g)
}

func removeDevice(dev *C.struct_udev_device) {
	devpath := C.udev_device_get_devnode(dev)
	if devpath == nil {
		return
	}

	path := C.GoString(devpath)

	deviceLock.Lock()
	defer deviceLock.Unlock()

	for i, g := range nextDevices {
		if g.path == path {
			nextDevices = append(nextDevices[:i], nextDevices[i+1:]...)

			_ = g.close()

			break
		}
	}
}

func update() error {
	// not needed on Linux
	return nil
}
