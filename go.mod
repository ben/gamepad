module git.lubar.me/ben/gamepad

go 1.16

require (
	golang.org/x/mobile v0.0.0-20210701032007-93619952ba7f
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c
	golang.org/x/tools v0.1.4
)
