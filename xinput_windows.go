// +build windows
//go:generate go run golang.org/x/sys/windows/mkwinsyscall -output zxinput_windows.go xinput_windows.go

package gamepad

import (
	"errors"
	"math"
	"syscall"
	"unsafe"
)

const (
	_ERROR_DEVICE_NOT_CONNECTED       = syscall.Errno(1167)
	_XUSER_MAX_COUNT                  = 4
	_XINPUT_GAMEPAD_TRIGGER_THRESHOLD = 30

	_XINPUT_GAMEPAD_DPAD_UP        = 0x0001
	_XINPUT_GAMEPAD_DPAD_DOWN      = 0x0002
	_XINPUT_GAMEPAD_DPAD_LEFT      = 0x0004
	_XINPUT_GAMEPAD_DPAD_RIGHT     = 0x0008
	_XINPUT_GAMEPAD_START          = 0x0010
	_XINPUT_GAMEPAD_BACK           = 0x0020
	_XINPUT_GAMEPAD_LEFT_THUMB     = 0x0040
	_XINPUT_GAMEPAD_RIGHT_THUMB    = 0x0080
	_XINPUT_GAMEPAD_LEFT_SHOULDER  = 0x0100
	_XINPUT_GAMEPAD_RIGHT_SHOULDER = 0x0200
	_XINPUT_GAMEPAD_GUIDE          = 0x0400
	_XINPUT_GAMEPAD_A              = 0x1000
	_XINPUT_GAMEPAD_B              = 0x2000
	_XINPUT_GAMEPAD_X              = 0x4000
	_XINPUT_GAMEPAD_Y              = 0x8000
)

type _XINPUT_STATE struct {
	dwPacketNumber uint32
	Gamepad        _XINPUT_GAMEPAD
}

type _XINPUT_GAMEPAD struct {
	wButtons          uint16
	bLeftTrigger      uint8
	bRightTrigger     uint8
	sThumbLX          int16
	sThumbLY          int16
	sThumbRX          int16
	sThumbRY          int16
	dwPaddingReserved uint32
}

var _XInputGetStateEx uintptr

func init() {
	_XInputEnable(1)

	_XInputGetStateEx = _GetProcAddress(modxinput1_4.Handle(), 100)
}

var controllers [_XUSER_MAX_COUNT]Gamepad

type gamepad struct {
	state _XINPUT_STATE
	err   error
	name  string
}

func (g *gamepad) getName() string {
	return "Xbox 360 Controller"
}

func (g *gamepad) numAxis() int {
	return 4
}

func (g *gamepad) numButton() int {
	return 17
}

func (g *gamepad) simpleButton(mask uint16) (float32, bool) {
	if g.state.Gamepad.wButtons&mask != 0 {
		return 1, true
	}

	return 0, false
}

func (g *gamepad) triggerButton(value uint8) (float32, bool) {
	return float32(value) / 255, value >= _XINPUT_GAMEPAD_TRIGGER_THRESHOLD
}

func (g *gamepad) getButton(index int) (float32, bool) {
	switch index {
	case 0:
		return g.simpleButton(_XINPUT_GAMEPAD_DPAD_UP)
	case 1:
		return g.simpleButton(_XINPUT_GAMEPAD_DPAD_DOWN)
	case 2:
		return g.simpleButton(_XINPUT_GAMEPAD_DPAD_LEFT)
	case 3:
		return g.simpleButton(_XINPUT_GAMEPAD_DPAD_RIGHT)
	case 4:
		return g.simpleButton(_XINPUT_GAMEPAD_START)
	case 5:
		return g.simpleButton(_XINPUT_GAMEPAD_BACK)
	case 6:
		return g.simpleButton(_XINPUT_GAMEPAD_LEFT_THUMB)
	case 7:
		return g.simpleButton(_XINPUT_GAMEPAD_RIGHT_THUMB)
	case 8:
		return g.simpleButton(_XINPUT_GAMEPAD_LEFT_SHOULDER)
	case 9:
		return g.simpleButton(_XINPUT_GAMEPAD_RIGHT_SHOULDER)
	case 10:
		return g.simpleButton(_XINPUT_GAMEPAD_GUIDE)
	case 11:
		return g.simpleButton(_XINPUT_GAMEPAD_A)
	case 12:
		return g.simpleButton(_XINPUT_GAMEPAD_B)
	case 13:
		return g.simpleButton(_XINPUT_GAMEPAD_X)
	case 14:
		return g.simpleButton(_XINPUT_GAMEPAD_Y)
	case 15:
		return g.triggerButton(g.state.Gamepad.bLeftTrigger)
	case 16:
		return g.triggerButton(g.state.Gamepad.bRightTrigger)
	default:
		return float32(math.NaN()), false
	}
}

func (g *gamepad) getAxis(index int) float32 {
	switch index {
	case 0:
		return ((float32(g.state.Gamepad.sThumbLX) + 32768) / 32767.5) - 1
	case 1:
		return 1 - ((float32(g.state.Gamepad.sThumbLY) + 32768) / 32767.5)
	case 2:
		return ((float32(g.state.Gamepad.sThumbRX) + 32768) / 32767.5) - 1
	case 3:
		return 1 - ((float32(g.state.Gamepad.sThumbRY) + 32768) / 32767.5)
	default:
		return float32(math.NaN())
	}
}

func (g *gamepad) indexToAxis(index int) Axis {
	switch index {
	case 0:
		return AxisLeftX
	case 1:
		return AxisLeftY
	case 2:
		return AxisRightX
	case 3:
		return AxisRightY
	default:
		return AxisUnknown
	}
}

func (g *gamepad) axisToIndex(axis Axis) int {
	switch axis {
	case AxisLeftX:
		return 0
	case AxisLeftY:
		return 1
	case AxisRightX:
		return 2
	case AxisRightY:
		return 3
	default:
		return -1
	}
}

func (g *gamepad) indexToButton(index int) Button {
	switch index {
	case 0:
		return BtnDUp
	case 1:
		return BtnDDown
	case 2:
		return BtnDLeft
	case 3:
		return BtnDRight
	case 4:
		return BtnStart
	case 5:
		return BtnSelect
	case 6:
		return BtnLeftStick
	case 7:
		return BtnRightStick
	case 8:
		return BtnL1
	case 9:
		return BtnR1
	case 10:
		return BtnMode
	case 11:
		return BtnA
	case 12:
		return BtnB
	case 13:
		return BtnX
	case 14:
		return BtnY
	case 15:
		return BtnL2
	case 16:
		return BtnR2
	default:
		return BtnUnknown
	}
}

func (g *gamepad) buttonToIndex(btn Button) int {
	switch btn {
	case BtnDUp:
		return 0
	case BtnDDown:
		return 1
	case BtnDLeft:
		return 2
	case BtnDRight:
		return 3
	case BtnStart:
		return 4
	case BtnSelect:
		return 5
	case BtnLeftStick:
		return 6
	case BtnRightStick:
		return 7
	case BtnL1:
		return 8
	case BtnR1:
		return 9
	case BtnMode:
		return 10
	case BtnA:
		return 11
	case BtnB:
		return 12
	case BtnX:
		return 13
	case BtnY:
		return 14
	case BtnL2:
		return 15
	case BtnR2:
		return 16
	default:
		return -1
	}
}

func (g *gamepad) update() error {
	return nil
}

func update() error {
	nextDevices = nextDevices[:0]
	for i := range controllers {
		var errno syscall.Errno

		if _XInputGetStateEx != 0 {
			r0, _, _ := syscall.Syscall(_XInputGetStateEx, 2, uintptr(i), uintptr(unsafe.Pointer(&controllers[i].state)), 0)
			errno = syscall.Errno(r0)
		} else {
			errno = _XInputGetState(uint32(i), &controllers[i].state)
		}

		if errno != 0 {
			controllers[i].err = errno
		} else {
			controllers[i].err = nil

			nextDevices = append(nextDevices, &controllers[i])
		}
	}

	for i := range controllers {
		if controllers[i].err != nil && !errors.Is(controllers[i].err, _ERROR_DEVICE_NOT_CONNECTED) {
			return controllers[i].err
		}
	}

	return nil
}

//sys _XInputEnable(enable uint32) = xinput1_4.XInputEnable
//sys _XInputGetState(dwUserIndex uint32, pState *_XINPUT_STATE) (errno syscall.Errno) = xinput1_4.XInputGetState
//sys _GetProcAddress(module uintptr, procName uintptr) (addr uintptr) = kernel32.GetProcAddress
