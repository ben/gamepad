// +build linux,!android

package gamepad

// #include <linux/joystick.h>
// inline int _JSIOCGNAME(int count) { return JSIOCGNAME(count); }
import "C"
import (
	"bytes"
	"errors"
	"math"
	"os"
	"runtime"
	"syscall"
	"unsafe"
)

var axisMap = [...]Axis{
	C.ABS_X:        AxisLeftX,
	C.ABS_Y:        AxisLeftY,
	C.ABS_Z:        AxisUnknown - 1, // LT
	C.ABS_RX:       AxisRightX,
	C.ABS_RY:       AxisRightY,
	C.ABS_RZ:       AxisUnknown - 1, // RT
	C.ABS_THROTTLE: AxisUnknown,
	C.ABS_RUDDER:   AxisUnknown,
	C.ABS_WHEEL:    AxisUnknown,
	C.ABS_GAS:      AxisUnknown,
	C.ABS_BRAKE:    AxisUnknown,
	C.ABS_HAT0X:    AxisUnknown - 1, // D-Pad
	C.ABS_HAT0Y:    AxisUnknown - 1, // D-Pad
	C.ABS_HAT1X:    AxisUnknown - 1,
	C.ABS_HAT1Y:    AxisUnknown - 1,
	C.ABS_HAT2X:    AxisUnknown - 1,
	C.ABS_HAT2Y:    AxisUnknown - 1,
	C.ABS_HAT3X:    AxisUnknown - 1,
	C.ABS_HAT3Y:    AxisUnknown - 1,
}

var analogButtonMap = [...][2]Button{
	C.ABS_Z:     {BtnUnknown - 1, BtnL2},
	C.ABS_RZ:    {BtnUnknown - 1, BtnR2},
	C.ABS_HAT0X: {BtnDLeft, BtnDRight},
	C.ABS_HAT0Y: {BtnDUp, BtnDDown},
	C.ABS_HAT1X: {BtnUnknown, BtnUnknown},
	C.ABS_HAT1Y: {BtnUnknown, BtnUnknown},
	C.ABS_HAT2X: {BtnUnknown, BtnUnknown},
	C.ABS_HAT2Y: {BtnUnknown, BtnUnknown},
	C.ABS_HAT3X: {BtnUnknown, BtnUnknown},
	C.ABS_HAT3Y: {BtnUnknown, BtnUnknown},
}

var buttonMap = [...]Button{
	C.BTN_0 - C.BTN_MISC:       BtnUnknown,
	C.BTN_1 - C.BTN_MISC:       BtnUnknown,
	C.BTN_2 - C.BTN_MISC:       BtnUnknown,
	C.BTN_3 - C.BTN_MISC:       BtnUnknown,
	C.BTN_4 - C.BTN_MISC:       BtnUnknown,
	C.BTN_5 - C.BTN_MISC:       BtnUnknown,
	C.BTN_6 - C.BTN_MISC:       BtnUnknown,
	C.BTN_7 - C.BTN_MISC:       BtnUnknown,
	C.BTN_8 - C.BTN_MISC:       BtnUnknown,
	C.BTN_9 - C.BTN_MISC:       BtnUnknown,
	0x10A - C.BTN_MISC:         BtnUnknown,
	0x10B - C.BTN_MISC:         BtnUnknown,
	0x10C - C.BTN_MISC:         BtnUnknown,
	0x10D - C.BTN_MISC:         BtnUnknown,
	0x10E - C.BTN_MISC:         BtnUnknown,
	0x10F - C.BTN_MISC:         BtnUnknown,
	C.BTN_LEFT - C.BTN_MISC:    BtnUnknown,
	C.BTN_RIGHT - C.BTN_MISC:   BtnUnknown,
	C.BTN_MIDDLE - C.BTN_MISC:  BtnUnknown,
	C.BTN_SIDE - C.BTN_MISC:    BtnUnknown,
	C.BTN_EXTRA - C.BTN_MISC:   BtnUnknown,
	C.BTN_FORWARD - C.BTN_MISC: BtnUnknown,
	C.BTN_BACK - C.BTN_MISC:    BtnUnknown,
	C.BTN_TASK - C.BTN_MISC:    BtnUnknown,
	0x118 - C.BTN_MISC:         BtnUnknown,
	0x119 - C.BTN_MISC:         BtnUnknown,
	0x11A - C.BTN_MISC:         BtnUnknown,
	0x11B - C.BTN_MISC:         BtnUnknown,
	0x11C - C.BTN_MISC:         BtnUnknown,
	0x11D - C.BTN_MISC:         BtnUnknown,
	0x11E - C.BTN_MISC:         BtnUnknown,
	0x11F - C.BTN_MISC:         BtnUnknown,
	C.BTN_TRIGGER - C.BTN_MISC: BtnUnknown,
	C.BTN_THUMB - C.BTN_MISC:   BtnL2,
	C.BTN_THUMB2 - C.BTN_MISC:  BtnR2,
	C.BTN_TOP - C.BTN_MISC:     BtnUnknown,
	C.BTN_TOP2 - C.BTN_MISC:    BtnUnknown,
	C.BTN_PINKIE - C.BTN_MISC:  BtnUnknown,
	C.BTN_BASE - C.BTN_MISC:    BtnUnknown,
	C.BTN_BASE2 - C.BTN_MISC:   BtnUnknown,
	C.BTN_BASE3 - C.BTN_MISC:   BtnUnknown,
	C.BTN_BASE4 - C.BTN_MISC:   BtnUnknown,
	C.BTN_BASE5 - C.BTN_MISC:   BtnUnknown,
	C.BTN_BASE6 - C.BTN_MISC:   BtnUnknown,
	C.BTN_DEAD - C.BTN_MISC:    BtnUnknown,
	C.BTN_A - C.BTN_MISC:       BtnA,
	C.BTN_B - C.BTN_MISC:       BtnB,
	C.BTN_C - C.BTN_MISC:       BtnUnknown,
	C.BTN_X - C.BTN_MISC:       BtnY,
	C.BTN_Y - C.BTN_MISC:       BtnX,
	C.BTN_Z - C.BTN_MISC:       BtnUnknown,
	C.BTN_TL - C.BTN_MISC:      BtnL1,
	C.BTN_TR - C.BTN_MISC:      BtnR1,
	C.BTN_TL2 - C.BTN_MISC:     BtnL2,
	C.BTN_TR2 - C.BTN_MISC:     BtnR2,
	C.BTN_SELECT - C.BTN_MISC:  BtnSelect,
	C.BTN_START - C.BTN_MISC:   BtnStart,
	C.BTN_MODE - C.BTN_MISC:    BtnMode,
	C.BTN_THUMBL - C.BTN_MISC:  BtnLeftStick,
	C.BTN_THUMBR - C.BTN_MISC:  BtnRightStick,
}

type gamepad struct {
	path      string
	fd        int
	version   uint32
	name      string
	vendor    string
	model     string
	axisMap   [C.ABS_CNT]uint8
	buttonMap [C.KEY_MAX - C.BTN_MISC + 1]uint16
	axis      []int16
	button    []bool
	axisIdx   []int
	axisBtn   []int
	closed    bool
}

func (g *gamepad) close() error {
	if g.closed {
		return os.ErrClosed
	}

	g.closed = true

	return syscall.Close(g.fd)
}

func (g *gamepad) ioctl(op uintptr, arg unsafe.Pointer) error {
	if g.closed {
		return os.ErrClosed
	}

	_, _, err := syscall.Syscall(syscall.SYS_IOCTL, uintptr(g.fd), op, uintptr(arg))

	return err
}

func open(path string) (*Gamepad, error) {
	fd, err := syscall.Open(path, syscall.O_NONBLOCK|syscall.O_RDONLY|syscall.O_CLOEXEC, 0400)
	if err != nil {
		return nil, err
	}

	g := &Gamepad{
		gamepad: gamepad{
			path: path,
			fd:   fd,
		},
	}

	runtime.SetFinalizer(g, func(g *Gamepad) {
		g.close()
	})

	err = g.ioctl(C.JSIOCGVERSION, unsafe.Pointer(&g.version))
	if err != nil {
		return nil, err
	}

	var (
		numAxes    uint8
		numButtons uint8
	)

	err = g.ioctl(C.JSIOCGAXES, unsafe.Pointer(&numAxes))
	if err != nil {
		return nil, err
	}

	g.axis = make([]int16, numAxes)

	err = g.ioctl(C.JSIOCGBUTTONS, unsafe.Pointer(&numButtons))
	if err != nil {
		return nil, err
	}

	g.button = make([]bool, numButtons)

	var gamepadName [256]byte

	err = g.ioctl(uintptr(C._JSIOCGNAME(4096)), unsafe.Pointer(&gamepadName[0]))
	if err != nil {
		return nil, err
	}

	i := bytes.IndexByte(gamepadName[:], 0)
	if i == -1 {
		i = len(gamepadName)
	}

	g.name = string(gamepadName[:i])

	err = g.ioctl(C.JSIOCGAXMAP, unsafe.Pointer(&g.axisMap[0]))
	if err != nil {
		return nil, err
	}

	err = g.ioctl(C.JSIOCGBTNMAP, unsafe.Pointer(&g.buttonMap[0]))
	if err != nil {
		return nil, err
	}

	g.axisBtn = make([]int, len(g.button))

	for i := 0; i < len(g.axisMap) && i < len(g.axis); i++ {
		if uint(g.axisMap[i]) >= uint(len(axisMap)) || axisMap[g.axisMap[i]] != AxisUnknown-1 {
			g.axisIdx = append(g.axisIdx, i)

			continue
		}

		g.addAxisButton(-i-1, analogButtonMap[g.axisMap[i]][0])
		g.addAxisButton(i+1, analogButtonMap[g.axisMap[i]][1])
	}

	return g, nil
}

func (g *gamepad) addAxisButton(axis int, btn Button) {
	switch btn {
	case BtnUnknown - 1:
		// ignored
	case BtnUnknown:
		g.axisBtn = append(g.axisBtn, axis)
	default:
		found := false

		for i, b := range g.buttonMap {
			if uint(i) >= uint(len(g.button)) {
				break
			}

			if uint(b-C.BTN_MISC) >= uint(len(buttonMap)) {
				continue
			}

			if buttonMap[b-C.BTN_MISC] == btn && g.axisBtn[i] == 0 {
				g.axisBtn[i] = axis
				found = true

				break
			}
		}

		if !found {
			g.axisBtn = append(g.axisBtn, axis)
		}
	}
}

func (g *gamepad) poll() (*C.struct_js_event, error) {
	if g.closed {
		return nil, os.ErrClosed
	}

	var event C.struct_js_event

	_, err := syscall.Read(g.fd, (*[unsafe.Sizeof(event)]byte)(unsafe.Pointer(&event))[:])
	if errors.Is(err, syscall.EAGAIN) {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}

	return &event, nil
}

func (g *gamepad) update() error {
	for {
		event, err := g.poll()
		if event == nil {
			return err
		}

		// number of milliseconds since arbitrary time origin
		_ = event.time

		// init event (synthesized to show initial position)
		_ = event._type&C.JS_EVENT_INIT != 0

		switch event._type &^ C.JS_EVENT_INIT {
		case C.JS_EVENT_BUTTON:
			switch event.value {
			case 0:
				g.button[event.number] = false
			case 1:
				g.button[event.number] = true
			}
		case C.JS_EVENT_AXIS:
			g.axis[event.number] = int16(event.value)
		}
	}
}

func (g *gamepad) getName() string {
	return g.vendor + "-" + g.model + "-" + g.name
}

func (g *gamepad) numAxis() int {
	return len(g.axisIdx)
}

func (g *gamepad) getAxis(index int) float32 {
	if uint(index) < uint(len(g.axisIdx)) {
		return float32(g.axis[g.axisIdx[index]]) / 32767
	}

	return float32(math.NaN())
}

func (g *gamepad) numButton() int {
	return len(g.axisBtn)
}

func (g *gamepad) getButton(index int) (float32, bool) {
	if uint(index) >= uint(len(g.axisBtn)) {
		return float32(math.NaN()), false
	}

	value, pressed := float32(0), false

	switch a := g.axisBtn[index]; {
	case a == 0:
		if g.button[index] {
			value = 1
		}
	case a < 0:
		if v := g.axis[1-a]; v < 0 {
			value = -float32(v) / 32767
		}
	default:
		if v := g.axis[a-1]; v > 0 {
			value = float32(v) / 32767
		}
	}

	if index < len(g.button) {
		pressed = g.button[index]
	} else {
		pressed = value >= 0.5
	}

	return value, pressed
}

func (g *gamepad) indexToAxis(index int) Axis {
	if uint(index) >= uint(len(g.axisIdx)) {
		return AxisUnknown
	}

	if uint(g.axisMap[g.axisIdx[index]]) < uint(len(axisMap)) {
		return axisMap[g.axisMap[g.axisIdx[index]]]
	}

	return AxisUnknown
}

func (g *gamepad) axisToIndex(axis Axis) int {
	if axis == AxisUnknown {
		return -1
	}

	for i, j := range g.axisIdx {
		if uint(g.axisMap[j]) < uint(len(axisMap)) && axisMap[g.axisMap[j]] == axis {
			return i
		}
	}

	return -1
}

func (g *gamepad) indexToButton(index int) Button {
	if uint(index) >= uint(len(g.axisBtn)) {
		return BtnUnknown
	}

	if uint(index) < uint(len(g.button)) {
		if uint(g.buttonMap[index]-C.BTN_MISC) < uint(len(buttonMap)) {
			return buttonMap[g.buttonMap[index]-C.BTN_MISC]
		}

		return BtnUnknown
	}

	var axis, dir int

	if g.axisBtn[index] < 0 {
		axis, dir = 1-g.axisBtn[index], 0
	} else {
		axis, dir = g.axisBtn[index]-1, 1
	}

	return analogButtonMap[g.axisMap[axis]][dir]
}

func (g *gamepad) buttonToIndex(btn Button) int {
	if btn == BtnUnknown {
		return -1
	}

	for i, axis := range g.axisBtn {
		switch {
		case axis == 0:
			if uint(g.buttonMap[i]-C.BTN_MISC) < uint(len(buttonMap)) && buttonMap[g.buttonMap[i]-C.BTN_MISC] == btn {
				return i
			}
		case axis < 0:
			if analogButtonMap[g.axisMap[1-axis]][0] == btn {
				return i
			}
		default:
			if analogButtonMap[g.axisMap[axis-1]][1] == btn {
				return i
			}
		}
	}

	return -1
}
