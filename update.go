package gamepad

import "sync"

var (
	devices     []*Gamepad
	nextDevices []*Gamepad
	deviceLock  sync.Mutex
)

// List returns the currently known gamepads.
//
// The returned slice is valid until Update is called.
func List() []*Gamepad {
	deviceLock.Lock()
	defer deviceLock.Unlock()

	return devices
}

// Update updates the state of the known gamepads.
//
// This function should be called before gamepad states are checked on each frame.
func Update() error {
	deviceLock.Lock()
	defer deviceLock.Unlock()

	if err := update(); err != nil {
		return err
	}

	different := len(devices) != len(nextDevices)
	if !different {
		for i, g := range devices {
			if g != nextDevices[i] {
				different = true

				break
			}
		}
	}

	if different {
		devices = append(devices[:0], nextDevices...)
	}

	var lastError error

	for _, g := range devices {
		if err := g.update(); err != nil {
			lastError = err
		}
	}

	return lastError
}
