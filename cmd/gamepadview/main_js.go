package main

import (
	"strconv"
	"syscall/js"

	"git.lubar.me/ben/gamepad"
)

var (
	frameCh     = make(chan struct{}, 1)
	rafCallback = js.FuncOf(func(js.Value, []js.Value) interface{} {
		frameCh <- struct{}{}

		return js.Undefined()
	})

	noGamepad js.Value
	axes      js.Value
	buttons   js.Value
)

func fatal(err error) {
	panic(err)
}

func initUI() error {
	noGamepad = js.Global().Get("document").Call("createElement", "div")
	noGamepad.Set("className", "gamepad none")
	noGamepad.Set("textContent", "No gamepad detected. If you have a gamepad connected, try pressing a button on it.")

	return nil
}

func nextFrame() bool {
	if axes.Truthy() {
		axes = js.Undefined()
		buttons = js.Undefined()
	} else {
		js.Global().Get("document").Get("body").Call("appendChild", noGamepad)
	}

	js.Global().Call("requestAnimationFrame", rafCallback)

	<-frameCh

	return true
}

func clearUI() {
	divs := js.Global().Get("document").Call("querySelectorAll", ".gamepad")

	for i := 0; i < divs.Length(); i++ {
		div := divs.Index(i)
		div.Get("parentNode").Call("removeChild", div)
	}
}

func startGamepad(name string) {
	doc := js.Global().Get("document")

	div := doc.Call("createElement", "div")
	div.Set("className", "gamepad")

	h1 := doc.Call("createElement", "h1")
	h1.Set("textContent", name)
	div.Call("appendChild", h1)

	axisTable := doc.Call("createElement", "table")
	axisTable.Set("className", "axes")
	div.Call("appendChild", axisTable)

	buttonTable := doc.Call("createElement", "table")
	buttonTable.Set("className", "buttons")
	div.Call("appendChild", buttonTable)

	thead := doc.Call("createElement", "thead")
	axisTable.Call("appendChild", thead)

	tr := doc.Call("createElement", "tr")
	thead.Call("appendChild", tr)

	th := doc.Call("createElement", "th")
	th.Set("textContent", "#")
	tr.Call("appendChild", th)

	th = doc.Call("createElement", "th")
	th.Set("colSpan", 2)
	th.Set("textContent", "Value")
	tr.Call("appendChild", th)

	th = doc.Call("createElement", "th")
	th.Set("textContent", "Name")
	tr.Call("appendChild", th)

	thead = doc.Call("createElement", "thead")
	buttonTable.Call("appendChild", thead)

	tr = doc.Call("createElement", "tr")
	thead.Call("appendChild", tr)

	th = doc.Call("createElement", "th")
	th.Set("textContent", "#")
	tr.Call("appendChild", th)

	th = doc.Call("createElement", "th")
	th.Set("textContent", "Value")
	th.Set("colSpan", 2)
	tr.Call("appendChild", th)

	th = doc.Call("createElement", "th")
	th.Set("textContent", "Pressed")
	tr.Call("appendChild", th)

	th = doc.Call("createElement", "th")
	th.Set("textContent", "Name")
	tr.Call("appendChild", th)

	axes = doc.Call("createElement", "tbody")
	axisTable.Call("appendChild", axes)

	buttons = doc.Call("createElement", "tbody")
	buttonTable.Call("appendChild", buttons)

	doc.Get("body").Call("appendChild", div)
}

func displayAxis(index int, value float32, axis gamepad.Axis) {
	doc := js.Global().Get("document")
	tr := doc.Call("createElement", "tr")

	td := doc.Call("createElement", "td")
	td.Set("textContent", index)
	tr.Call("appendChild", td)

	td = doc.Call("createElement", "td")
	input := doc.Call("createElement", "input")
	input.Set("type", "range")
	input.Set("min", -1)
	input.Set("max", 1)
	input.Set("value", value)
	input.Set("disabled", true)
	td.Call("appendChild", input)
	tr.Call("appendChild", td)

	td = doc.Call("createElement", "td")
	td.Set("textContent", strconv.FormatFloat(float64(value), 'f', 6, 32))
	tr.Call("appendChild", td)

	td = doc.Call("createElement", "td")
	td.Set("textContent", axis.String())
	tr.Call("appendChild", td)

	axes.Call("appendChild", tr)
}

func displayButton(index int, value float32, pressed bool, button gamepad.Button) {
	doc := js.Global().Get("document")
	tr := doc.Call("createElement", "tr")

	td := doc.Call("createElement", "td")
	td.Set("textContent", index)
	tr.Call("appendChild", td)

	td = doc.Call("createElement", "td")
	input := doc.Call("createElement", "input")
	input.Set("type", "range")
	input.Set("min", 0)
	input.Set("max", 1)
	input.Set("value", value)
	input.Set("disabled", true)
	td.Call("appendChild", input)
	tr.Call("appendChild", td)

	td = doc.Call("createElement", "td")
	td.Set("textContent", strconv.FormatFloat(float64(value), 'f', 6, 32))
	tr.Call("appendChild", td)

	td = doc.Call("createElement", "td")
	input = doc.Call("createElement", "input")
	input.Set("type", "checkbox")
	input.Set("checked", pressed)
	input.Set("disabled", true)
	td.Call("appendChild", input)
	tr.Call("appendChild", td)

	td = doc.Call("createElement", "td")
	td.Set("textContent", button.String())
	tr.Call("appendChild", td)

	buttons.Call("appendChild", tr)
}
