package main

import "git.lubar.me/ben/gamepad"

func main() {
	if err := initUI(); err != nil {
		fatal(err)
	}

	for nextFrame() {
		if err := gamepad.Update(); err != nil {
			fatal(err)
		}

		clearUI()

		for _, g := range gamepad.List() {
			startGamepad(g.Name())

			for a := 0; a < g.NumAxis(); a++ {
				displayAxis(a, g.Axis(a), g.IndexToAxis(a))
			}

			for b := 0; b < g.NumButton(); b++ {
				value, pressed := g.Button(b)
				displayButton(b, value, pressed, g.IndexToButton(b))
			}
		}
	}
}
